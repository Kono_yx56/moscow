package org.itstep.qa;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

public class MoscowRunner {


    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");

// создаем экземпляр вебдрайвера

        WebDriver driver = new ChromeDriver();
        // открываем нужную страницу

        driver.navigate().to("http://hflabs.github.io/suggestions-demo/");

        WebElement webElement = driver.findElement(By.xpath("//*[@id=\"fullname\"]"));

        webElement.sendKeys("Иванов Иван Иванович" + Keys.ENTER);
        webElement.click();
        webElement = driver.findElement(By.xpath("//*[@id=\"email\"]"));
        webElement.sendKeys("me@example.com" + Keys.ENTER);
        webElement.click();
        webElement = driver.findElement(By.xpath("//*[@id=\"message\"]"));
        webElement.sendKeys("Hallo" + Keys.ENTER);
        webElement.click();
        webElement = driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button"));
        webElement.click();


        try {
            Thread.sleep(5000);

        } catch (InterruptedException e) {
            e.printStackTrace();

            webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]"));
            webElement = driver.findElement(By.xpath("//*[@id=\"feedback-message\"]/p[2]/button"));
            webElement.click();

            Assert.assertEquals(webElement.getText(), ("Извините, у нас обед"));

            Assert.assertEquals(webElement.getText(), ("Это не настоящее правительство :-(\n" +
                    "                К сожалению, мы не можем принять ваше обращение.\n" +
                    "                Но вы всегда можете отправить его через электронную приемную правительства Москвы"));

            Assert.assertEquals(webElement.getText(), ("Хорошо, я понял"));

        }}}

